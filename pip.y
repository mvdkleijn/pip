/* Copyright 2013, Martijn van der Kleijn

    $ ./lemon pip.y                          

  The above statement will create pip.c.

  The command below  adds  main and the
  necessary "Parse" calls to the
  end of this pip.c.


    $ cat <<EOF >>pip.c                      
    int main()                                    
    {                                             
      void* pParser = ParseAlloc (malloc);        
      Parse (pParser, INTEGER, 1);                
      Parse (pParser, PLUS, 0);                   
      Parse (pParser, INTEGER, 2);                
      Parse (pParser, 0, 0);                      
      ParseFree(pParser, free );                  
     }                                            
    EOF                                           
            

     $ g++ -o ex1 example1.c                                      
     $ ./ex1

  See the Makefile, as most all of this is
  done automatically.
  
*/

%token_type {char*}
   
%include {
#include "stdlib.h"
#include "assert.h"
#include "pip.h"
}  
   
%syntax_error {  
  printf("Syntax error!");
}

program ::= statement.

statement ::= ID.
   
expr(A) ::= expr(B) MINUS  expr(C).   { A = B - C; }  
expr(A) ::= expr(B) PLUS  expr(C).   { A = B + C; }  
expr(A) ::= expr(B) TIMES  expr(C).   { A = B * C; }  
expr(A) ::= expr(B) DIVIDE expr(C).  { 

         if(C != 0){
           A = B / C;
          }else{
           printf("divide by zero");
           }
}  /* end of DIVIDE */

expr(A) ::= INTEGER(B). { A = B; }
