/* Pip 0.1 */

%{
/* need this for the call to atof() below */
#include <math.h>
#include "pip.h"

int line_num = 1;
int column = 0;

void count();

void pr(char* msg, char* val) {
    printf("%s (%s) \n", msg, val);
}
%}

%option reentrant
%option noyywrap

%x COMMENTS
%x STRING

%%

"if"                    { count(); pr("IF", yytext); }
"else"                  { count(); pr("ELSE", yytext); }
"while"                 { count(); pr("WHILE", yytext); }
"do"                    { count(); pr("DO", yytext); }
"for"                   { count(); pr("FOR", yytext); }
"foreach"               { count(); pr("FOREACH", yytext); }
"switch"                { count(); pr("SWITCH", yytext); }
"case"                  { count(); pr("CASE", yytext); }
"this"                  { count(); pr("THIS", yytext); }
"self"                  { count(); pr("SELF", yytext); }

"<\?pip"                { count(); pr("START_PIP", yytext); }
"\?>"                   { count(); pr("END_PIP", yytext); }
"\""                    { count(); pr("DOUBLE_QUOTE", yytext); }
"\\'"                   { count(); pr("QUOTE_ESC", yytext); }
"\\\""                  { count(); pr("DOUBLE_QUOTE_ESC", yytext); }
"${"                    { count(); pr("INLINEVAR_OPEN", yytext); }
","                     { count(); pr("COMMA", yytext); }
"."                     { count(); pr("DOT", yytext); }
";"                     { count(); pr("COLON", yytext); }
"'"                     { count(); pr("QUOTE", yytext); }
"("                     { count(); pr("PARENTHESES_OPEN", yytext); }
"{"                     { count(); pr("BRACES_OPEN", yytext); }
"["                     { count(); pr("BRACKETS_OPEN", yytext); }
")"                     { count(); pr("PARENTHESES_CLOSE", yytext); }
"}"                     { count(); pr("BRACES_CLOSE", yytext); }
"]"                     { count(); pr("BRACKETS_CLOSE", yytext); }
">"                     { count(); pr("GT", yytext); }
"<"                     { count(); pr("LT", yytext); }
"+"                     { count(); pr("PLUS_OP", yytext); }
"-"                     { count(); pr("MINUS_OP", yytext); }
"*"                     { count(); pr("MULTI_OP", yytext); }
"/"                     { count(); pr("DIV_OP", yytext); }
"<="                    { count(); pr("LE_OP", yytext); }
">="                    { count(); pr("GE_OP", yytext); }
"=="                    { count(); pr("EQ_OP", yytext); }
"!="                    { count(); pr("NE_OP", yytext); }
"++"                    { count(); pr("INC_OP", yytext); }
"--"                    { count(); pr("DEC_OP", yytext); }
"&&"                    { count(); pr("AND_OP", yytext); }
"||"                    { count(); pr("OR_OP", yytext); }
"+="                    { count(); pr("ADD_ASSIGN", yytext); }
"-="                    { count(); pr("SUB_ASSIGN", yytext); }
"*="                    { count(); pr("MUL_ASSIGN", yytext); }
"/="                    { count(); pr("DIV_ASSIGN", yytext); }
"%="                    { count(); pr("MOD_ASSIGN", yytext); }
"&="                    { count(); pr("AND_ASSIGN", yytext); }
"^="                    { count(); pr("XOR_ASSIGN", yytext); }
"|="                    { count(); pr("OR_ASSIGN", yytext); }
"="                     { count(); pr("ASSIGN", yytext); }

[0-9]                   { count(); pr("DIGIT", yytext); }
[0-9]+                  { count(); pr("INT", yytext); }
[0-9]*"\."[0-9]*        { count(); pr("FLOAT", yytext); }
L?\'(\\.|[^\\'])*\'     { count(); pr("STRING_LITERAL", yytext); }
L?\"(\\.|[^\\"])*\"     { count(); pr("STRING_LITERAL", yytext); }
[_a-zA-Z][_a-zA-Z0-9]*  { count(); pr("ID", yytext); }

"\/\/"[^}\n]*           { count(); pr("COMMENT LINE", yytext); } /* eat up one-line comments */
"\/\*"                  { count(); BEGIN(COMMENTS); pr("COMMENT START", yytext); } /* start of a comment: go to a 'COMMENTS' state. */
<COMMENTS>\*\/          { count(); BEGIN(INITIAL); pr("COMMENT END", yytext); } /* end of a comment: go back to normal parsing. */
<COMMENTS>("\n"|"\r\n") { count(); ++line_num; }   // still have to increment line numbers inside of comments!
<COMMENTS>.             { count(); } // ignore every other character while we're in this state
[ \t\v\f]               { count(); } /* eat up whitespace */
("\n"|"\r\n")           { count(); ++line_num; }
.                       { printf("Unrecognized character (%s) on line %i | ", yytext, line_num); }

%%

void count() {
	int i;

	for (i = 0; yytext[i] != '\0'; i++)
		if (yytext[i] == '\n')
			column = 0;
		else if (yytext[i] == '\t')
			column += 8 - (column % 8);
		else
			column++;

	//ECHO;
}

main( argc, argv )
int argc;
char **argv;
    {
    ++argv, --argc;  /* skip over program name */
    if ( argc > 0 )
            yyin = fopen( argv[0], "r" );
    else
            yyin = stdin;

    yylex();
    printf("\n# of lines = %d, # of chars = %d\n", line_num, column);
    }
